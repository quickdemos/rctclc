import React from 'react';
import Paragraph from './Paragraph';
import Calculation from '../logic/calculation';

export default class Layout extends React.Component {
    constructor(props) {
        super(props);

        // initial state
        this.state = {
            content: '',
            value: ''
        };

        // bind methods
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleChange(event) {
        event.preventDefault();

        console.log('   handleChange', event.target.value);
        this.setState({
            value: event.target.value
        });

    }

    handleSubmit(event) {
        event.preventDefault();

        let calculation = new Calculation(this.state.value);
        console.log(" #$#$  calculation", calculation.calculate(), " expression: ", calculation.expression);

        let result = calculation.calculate();

        let wrongInputWarning = 'Wrong input!';

        this.setState({
            content: wrongInputWarning
        });

        if (result !== false) {
            let pattern = /(\d+\.\d+|\d+)\s*([+\-*/])\s*(\d+\.\d+|\d+)/;
            let matches = pattern.exec(calculation.expression);

            result = `${matches[1]} ${matches[2]} ${matches[3]} = ${result}`;

            this.setState({
                content: result
            });

        }
    }

    render() {
        return (
            <div>
                <div className="row">
                    <h1 className="col-md-8 col-md-offset-2 text-center">DevSkiller React Calculator</h1>
                </div>

                <div className="container">
                    <div className="row">
                        <form className="col-md-6 col-md-offset-3 text-center" onSubmit={this.handleSubmit}>
                            <input type="text" className="form-control col-md-9" placeholder="expression..."
                                   onChange={this.handleChange}/>
                            <input className="btn btn-success" type="submit" value="Submit"/>
                        </form>
                    </div>

                    <Paragraph content={this.state.content} />
                </div>
            </div>
        )
    }
}
