export default class Calculation {
    constructor(expression) {
        this.expression = expression;
        this.a = 0;
        this.b = 0;
        this.sign = '';
    }

    addition() {
        return this.a + this.b;
    }

    subtraction() {
        return this.a - this.b;
    }

    production() {
        return this.a * this.b;
    }

    division() {
        return this.a / this.b;
    }

    calculate() {
          let pattern = /(\d+\.\d+|\d+)\s*([+\-*/])\s*(\d+\.\d+|\d+)/;  /* @TODO Add regular expression */;

        if (this.expression.match(pattern)) {
            let matches = pattern.exec(this.expression);

            // extract the symbol sign from position 3
            this.sign = matches[2];
            // extract the A and B  from  match positions 1 and 4
            this.a = parseFloat(matches[1]);
            this.b = parseFloat(matches[3]);




            let result = false;

            switch (this.sign) {
                case '+':
                    result = this.addition();
                    break;
                case '-':
                    result = this.subtraction();
                    break;
                case '*':
                    result = this.production();
                    break;
                case '/':
                    result = this.division();
                    break;
            }

            return result;
        }
        else {
            return false;
        }
    }
}
