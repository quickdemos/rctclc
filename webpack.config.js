const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const isProd = process.env.NODE_ENV === 'production';

module.exports = {
    context: __dirname + '/src',
    entry: './index.js',
    output: {
        filename: '[name].js',
        path: path.join(__dirname,'web'),
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['react', 'es2015', 'stage-0'],
                        plugins: ['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy'],
                    }
                }
            }
        ]
    },
    plugins: [
          new HtmlWebpackPlugin({
                inject: true,
                template: 'index.html',
                minify: isProd ? { caseSensitive: true, collapseWhitespace: true } : false,
                hash: isProd,
          }),
    ],
    devServer: {
          compress: false,
          port: 3000,
          hot: false,
    }
};
